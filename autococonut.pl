#!/usr/bin/perl

# Copyright 2023, Red Hat, Inc
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Authors:
#   Lukáš Růžička <lruzicka@redhat.com>
#


use warnings;
# FIXME Rewrite using Smart::Match
use experimental "smartmatch";
use strict;
use YAML::Syck;
use feature qw(say);
use Data::Dumper qw(Dumper);


sub event_names {
# This routine takes the key code and the type of such code (key or mouse)
# and translates the code into a key name that is human readable. 
# Currently the codes are hardcoded here, but that might change in the
# future if it is necessary. When there is no key code definition,
# the name "unsupported" is returned instead. This should not, however,
# happen on a regular US keyboard with the US layout, that is the only
# supported one at the moment.
    my ($code, $type) = @_;
    my $readable = undef;
    my %keycodes = (1, 'escape',
            2 =>  '1', 3 =>  '2', 4 =>  '3', 5 =>  '4', 6 =>  '5', 7 =>  '6', 8 =>  '7', 9 =>  '8',
            10 =>  '9', 11 =>  '0', 12 =>  'minus', 13 =>  'equal', 14 =>  'backspace', 15 =>  'tab',
            16 =>  'q', 17 =>  'w', 18 =>  'e', 19 =>  'r', 20 =>  't', 21 =>  'y', 22 =>  'u',
            23 =>  'i', 24 =>  'o', 25 =>  'p', 26 =>  '(', 27 =>  ')', 28 =>  'enter', 29 =>  'leftcontrol',
            30 =>  'a', 31 =>  's', 32 =>  'd', 33 =>  'f', 34 =>  'g', 35 =>  'h', 36 =>  'j',
            37 =>  'k', 38 =>  'l', 39 =>  'semicolon', 40 =>  "apostrophe", 41 =>  'grave', 42 =>  'leftshift',
            43 =>  'backslash', 44 =>  'z', 45 =>  'x', 46 => 'c', 47 => 'v', 48 => 'b', 49 => 'n',
            50 => 'm', 51 => ',', 52 => '.', 53 => 'slash', 54 => 'rightshift', 55 => 'kpasterisk',
            56 => 'leftalt', 57 => 'space', 58 => 'capslock', 59 => 'f1', 60 => 'f2', 61 => 'f3',
            62 => 'f4', 63 => 'f5', 64 => 'f6', 65 => 'f7', 66 => 'f8', 67 => 'f9' , 68 => 'f10', 
            69 => 'numlock', 70 => 'scrollock' , 71 => 'kp7', 72 => 'kp8' , 73 => 'kp9', 74 => 'kpminus',
            75 => 'kp4', 76 => 'kp5', 77 => 'kp6', 78 => 'kpplus', 79 => 'kp1', 80 => 'kp2', 81 => 'kp3',
            82 => 'kp0', 83 => 'kpdot', 85 => 'zenkakuhankaku', 86 => '102nd', 87 => 'f11', 88 => 'f12',
            89 => 'ro', 90 => 'katakana', 91 => 'hiragana', 97 => 'rightcontrol',  98 => 'kpslash',  
            99 => 'sysrq',  100 => 'rightalt', 102 => 'home', 103 => 'up', 104 => 'pageup', 105 => 'left',
            106 => 'right', 107 => 'end', 108 => 'down', 109 => 'pagedown', 110 => 'insert', 111 => 'delete',
            113 => 'mute', 114 => 'volumedown', 115 => 'volumeup', 116 => 'power', 117 => 'kpequal', 
            119 => 'pause', 121 => 'kpcomma', 125 => 'leftmeta', 126 => 'rightmeta',  122 => 'hengeul',
            123 => 'hanja', 124 => 'yen', 127 => 'compose', 128 => 'stop', 129 => 'again', 130 => 'props',
            131 => 'undo', 132 => 'front', 133 => 'copy', 134 => 'open', 135 => 'paste', 136 => 'find',
            137 => 'cut', 138 => 'help', 92 => 'henkan', 93 => 'katakanahiragana', 94 => 'muhenkan',
            95 => 'kpjpcomma', 96 => 'kpenter', 272 => 'leftmousebutton', 273 => 'rightmousebutton',
            274 => 'middlemousebutton', 275 => 'sidemousebutton');

    my %mousecodes = (0 => 'xmove', 1 => 'ymove', 11 => 'wheel');

    my @keys;
    # Depending on the type of the code, different key sets will be used.
    if ($type eq "key") {
        @keys = keys %keycodes; 
    }
    else {
        @keys = keys %mousecodes;
    }
    
    # This part makes the existence checking for the key codes.
    if ($code ~~ @keys) { # Smartmatch -> $keycode exists in @keys
        $readable = $keycodes{$code};
        $readable = $mousecodes{$code} if ($type ne "key");
        return $readable;
    }
    else {
        return "unsupported";
    }
}

sub load_yaml_from_disk {
# The data for this script come from a libinput recording. Such recording
# can be obtained by using the following command:
#
#  $ sudo libinput record /dev/input/eventX /dev/input/eventY -o recording.yml --show-keycodes
#
# The devices are the devices from which you want to obtain the input, such as keyboard and mouse
# (check for libinput --help to see more details). The recording must be run with sudo, because 
# regular users do not have access to the devices and they cannot be captured. 
# The --show-keycodes option is necessary, otherwise the keys and buttons will be obfuscated, 
# so you will not be able to tell which key was pressed.
#
# The recording is a YAML file, so we will use the YAML parser to get it.
    my $path = shift;
    my $yaml_file = LoadFile($path);
    return $yaml_file;    
}

sub name_moves {
# This routine renames the numerical representation of moving the mouse.
# It takes a duplet with X and Y values and returns a string symbolizing
# the direction: north, east, south, west, and combinations. 
# Mouse moves themselves do not have any importance, except for mouse drags,
# so we only want to see the direction as there is no way how we could
# get the coordinates under Wayland anyway.
# 
# All mouse moves are added to the starting point [0, 0], so after the move
# is ended we will get a duplet with X and Y values that are either positive
# or negative. On X scale, positive values are to the south and negative
# to the north, while on Y scale the former go to east and the latter to
# west. Merged directions, e.g. southwest, are also possible.
    my ($type, $direction) = @_;
    my ($xaxis, $yaxis);
    if ($type eq "mousewheel") {
        $xaxis = "up";
        $xaxis = "down" if ($direction->[0] < 0);
        return $xaxis;
    }
    elsif ($type eq "mousemove") {
        $xaxis = "";
        $yaxis = "";
        $xaxis = "left" if ($direction->[0] < 0);
        $xaxis = "right" if ($direction->[0] > 0);
        $yaxis = "up" if ($direction->[1] > 0);
        $yaxis = "down" if ($direction->[1] < 0);
        return "$xaxis-$yaxis";
    }
}

sub add_to_move {
    # This is a helper to add the current move to the existing
    # dublet. The $direction is the single movement dublet and
    # $moves are the total movements.
    my ($moves, $direction) = @_;
    $moves->[0] += $direction->[0];
    $moves->[1] += $direction->[1];
    return $moves;
}

sub handle_special_keys {
    # On a keyboard, we consider some of the keys being special.
    # Typically, these keys do not type anything, but they have various
    # kinds of actions - moving cursor, switching modes, etc.
    # At the same time, they are not modifier keys and they can be 
    # involved in constituting modified key combos.
    # Modifiers are keys that are typically used to create various
    # key combinations with specific functions in the operating
    # system.
    #
    # This routine takes the name of the key and its action (press, release)
    # and creates a key event that will later be used for the interpretation
    # of the events.
    my ($key, $action) = @_;
    # We take the following keys as modifiers.
    my @modifiers = qw(rightcontrol leftcontrol leftalt rightalt rightshift leftshift leftmeta rightmeta);
    # And these are special keys.
    my @specials = qw(enter f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 home end up left right down
                      pageup pagedown insert delete scrollock pause sysrq tab);

    # The recording gives us the key code and its action (press, release) only,
    # so we want to make it more specific, therefore add various prefixes
    # to the actions.
    # For mouse buttons
    if (index($key, "mouse") != -1) {
        $action = "mouse".$action;
    }
    # For modifiers
    elsif ($key ~~ @modifiers) {
        $action = "mod".$action;
    }
    # For special keys
    elsif ($key ~~ @specials) {
        $action = "spec".$action;
    }
    # For regular keys
    else {
        $action = "key".$action;
    }
    # This should not happen, but for all other cases
    # leave the action as is.
    return $action;
}


sub parse_recording {
# This is the main routin to parse the actual yaml recording that is obtained from
# libinput. It will iterate over the single events, it will simplify them reasonably
# and store them in a hash where a time stamp is used as a key and the event (array)
# is used as a value.
    my $data = shift;
    my $simplified = {};
    # The YAML parser return the object as a bunch of references, so we need to dereference. 
    my $devices = $data->{devices};
    my @devices = @$devices;
    # The main iteration begins here.
    for my $device (@devices) { 
        my $events = $device->{events};
        # Libinput can record any possible device connected to the computer. 
        # If a device was chosen to be recorded, but it did not produce any
        # output to be recorded, the device will have no $events. If that is
        # the case, skip to the next device and do not bother with this one.
        next unless ($events);
        my @events = @$events;
        # Iterate over the list of events and collect some of the info.
        for my $event (@events) {
            # Details are part of the evdev keyword.
            my $details = $event->{evdev};
            # The time stamp is provided with two values. The first is a whole number
            # in seconds from the start of the recording, while the other is a fraction
            # number in between the whole seconds.
            my $wholesec = $details->[0][0];
            my $fragsec = $details->[0][1];
            # The fraction part does not have leading zeroes and therefore the values
            # cannot be compared simply, as this would lead to time mismatch. Instead,
            # we need to restore the leading zeroes first, before creating the actual
            # time stamp.
            # The fraction is maximally 6 digits long, so we will detract the actual
            # length from the maximal value and we will get the count of zeroes that
            # we need to add before the fraction.
            my $stamplen = length($fragsec);
            my $difference = 6 - $stamplen;
            my $zeroes = "0" x $difference;
            $fragsec = $zeroes . $fragsec;
            my $timestamp = "$wholesec.$fragsec";
            # Now, we shall collect the action details from the event. 
            # The type of the event is represented by a number.
            # 2 represents mouse movements and wheel, 4 is for buttons and keys.
            my $type = $details->[0][2];
            # Let us categorize keyboard events.
            my $info;
            if ($type == 4) {
                my $keycode = $details->[1][3]; # Gets the keycode (dereferencing array).
                my $keyname = event_names($keycode, "key"); # Changes it into a human readable name.
                my $keypress = $details->[1][4]; # Gets if pressed or released.
                # Assume the action is pressed unless stated otherwise.
                my $keyaction = "press";
                if ($keypress == 0) {$keyaction = "release"};
                # For special keys, we need to do some more sorting in order to have various
                # keynames for later interpretations.
                $keyaction = handle_special_keys($keyname, $keyaction);
                # Save the events into the new hash
                $info = [$keyaction, $keyname];
                $simplified->{$timestamp} = $info;
            }
            # For mouse events.
            elsif ($type == 2) {
                # The mouse moves have either two or three entries - when mouse moves
                # in one direction, it will have two entries, but when it moves in
                # two directions, it will have three. We need to know this to be able
                # to read correct info. The mouse wheel activity also has three lines.
                my $nlines = scalar @$details;
                my ($xmove, $ymove, $wmove);
                if ($nlines == 3) {
                    # In case of three lined events, we need to know whether we are
                    # dealing with a mouse move or a wheel scroll. The wheel scroll
                    # returns the number 8 in its details, while the mouse move does not.
                    # So if 8 is not in the details, we will record two way movement, 
                    # otherwise we will record a one way movement.
                    if ($details->[0][3] != 8) {
                        # It seems the xmove is on the first line and ymove on the second
                        $xmove = $details->[0][4];
                        $ymove = $details->[1][4];
                    }
                    else {
                        $wmove = $details->[0][4];
                    }
                }
                elsif ($nlines == 2) {
                    # If the value of the event is 0, it is an xmove.
                    if ($details->[0][3] == 0) {
                        $xmove = $details->[0][4];
                        $ymove = 0;
                    }
                    # With 1, it is an ymove.
                    elsif ($details->[0][3] == 1) {
                        $xmove = 0;
                        $ymove = $details->[0][4];
                    }
                    # When it is something else, we do not support this yet,
                    # so do as if nothing happened.
                    else {
                        $xmove = 0;
                        $ymove = 0;
                        say("An unexpected move has emerged. It will not be recorded.");
                    }
                }
                else {
                    say("An unexpected event has emerged. It will not recorded.");
                }
                
                # We will now create the event with $action and $xaxis, $yaxis moves.
                my ($action, $xaxis, $yaxis);
                # For mouse wheel actions we only need to know if the wheel
                # moves north (up) or south (down) along the X axis:
                if ($wmove) {
                    $action = "mousewheel";
                    my $direction = [$wmove, 0];
                    $info = [$action, $direction];
                    $simplified->{$timestamp} = $info;
                }
                # For mouse moves actions, we need to know both axes in
                # order to know the exact directions north, south, east, west.
                else {
                    $action = "mousemove";
                    my $direction = [$xmove, $ymove];
                    $info = [$action, $direction];
                    $simplified->{$timestamp} = $info;
                }
            }
        }
    }
    return $simplified;
}

sub compare_events {
# This is a helper function to compare two events. It takes the name
# of the $last action and compare it two $current action.
    my ($last, $current) = @_;
    if ($last->[0] eq $current->[0]) {
        return 1;
    }
    else {
        return 0;
    }
}

sub use_text_buffer {
# This is a helper function to use the text buffer where key events
# place their values to be collected when a meta event, i.e. an event
# that consists of more than one events, finishes. For example, all
# regular keys place themselves in the text buffer and they will be 
# collected at once by another events as "typing" and not just as
# single key presses.
    my ($status, $key, $textbuffer) = @_;
    # Some keys return its name, but they have a different textual
    # meaning and they need to be rewritten to make the typing string
    # correctly. This will be handled here. If a key need to be 
    # rewritten, the rule should be placed in the following hash:
    my %rewrites = ("space", " ", "dot", ".");
    # For typing purposes, we will not record BACKSPACE, but we will
    # make it delete the last character in the text buffer, because
    # we want to be able to correct typos made during the recording.
    # However, if a modifier has been pressed before, we will treat
    # backspace as a special key.
    if (($key eq "backspace") and ($status->{modified} == 0)) {
        # Delete the last character from the buffer, if it is
        # not already empty.
        if (scalar @$textbuffer > 0) {
            pop(@$textbuffer);
        }  
    }
    # If the key has a rewrite rule, rewrite it.
    elsif (exists($rewrites{$key})) {
        push(@$textbuffer, $rewrites{$key});
    }
    # Or just put the key name at the end of the list.
    else {
        push(@$textbuffer, $key);
    }
    return $textbuffer;
}

sub action_into_status {
# This is a simple wrapper to push an array
# into another array in a hash.
    my ($status, $action) = @_;
    my $interpreted = $status->{interpreted};
    push(@$interpreted, $action);
    $status->{interpreted} = $interpreted;
    return $status;
}

# This routine will check if there are mouse events that remain 
# left behind and it will collect them. It will behave differently 
# for mouse moves and mouse scrolls. It will read the recorded 
# total movement, rename its direction, delete the buffers, and
# return the results.
sub collect_previous_mouse {
    my ($data, $status, $lastkey) = @_;
    my $lastaction = $data->{$lastkey}[0];
    # We confirm that the last action is a mousemove or
    # mousewheel.
    if ($lastaction eq "mousemove") {
        my $move = $status->{moves};
        my $direction = name_moves("mousemove", $move);
        $status->{moves} = [0, 0];
        if ($direction) {
            my $drag = $status->{drag};
            # If we are not in the drag mode, then we will 
            # record a mouse move. Otherwise, we will do
            # nothing as the drag will be recorded when
            # the mouse button is released.
            unless ($drag) {
                my $action = [$lastkey, "mousemove", $direction];
                $status = action_into_status($status, $action);
            }
        }
    }
    elsif ($lastaction eq "mousewheel") {
        my $scroll = $status->{scrolls};
        my $direction = name_moves("mousewheel", $scroll);
        $status->{scrolls} = [0, 0];
        if ($direction) {
            my $action = [$lastkey, "mousescroll", $direction];
            $status = action_into_status($status, $action);
        }
    }
    return $status;
}

sub collect_previous_click {
# This routine will collect a previous mouse click. According to the
# current mouse click buffer, there will be a certain number of clicks.
# At this time, we will only recognize a single click, a double click,
# and a tripple click, because these are also recognized in openQA.
# Any other number of consecutive clicks might be problematic as the buffer
# will get deleted in between them, which means that they might be split
# into several double clicks or tripple clicks.
    my ($data, $status, $lastkey) = @_;
    my $clicks = $status->{clicks};
    my $lastvalue = $data->{$lastkey}[1];
    my $ccount = scalar @$clicks;
    if ($ccount >= 3) {
        my $action = [$lastkey, "mouseclick_tripple", $lastvalue];
        $status = action_into_status($status, $action);
    }
    elsif ($ccount == 2) {
        my $action = [$lastkey, "mouseclick_double", $lastvalue];
        $status = action_into_status($status, $action);
    }
    elsif ($ccount == 1) {
        my $action = [$lastkey, "mouseclick_single", $lastvalue];
        $status = action_into_status($status, $action);
    }
    $status->{clicks} = [];
    return $status;
}

sub collect_previous_keys {
# This routine will collect a previous key event and return
# the content of the text buffer.
    my ($data, $status, $lastkey) = @_;
    my $lastaction = $data->{$lastkey}[0];
    my $textbuffer = $status->{textbuffer};
    my $text = join("", @$textbuffer);
    my $name = "typetext";
    $name = "keypressed" if (scalar(@$textbuffer) == 1);
    $status->{textbuffer} = [];
    if ($text) {
        my $action = [$lastkey, $name, $text];
        $status = action_into_status($status, $action);
    }
    return $status;
}

sub resolve_keypress {
    # This routine is called from the main interpreting function and it will
    # deal with a simple keypress.
    my ($data, $status, $lastkey, $currentkey) = @_;
    # Read the actions for both, the last, and the current events, as well as their
    # values.
    my ($currentaction, $currentvalue) = ($data->{$currentkey}[0], $data->{$currentkey}[1]);
    my ($lastaction, $lastvalue) = ($data->{$lastkey}[0], $data->{$lastkey}[1]);
    # Before we will deal with the key alone, we will collect the previous mouse events
    # as mouse events are always interrupted by keyboard events.
    $status = collect_previous_mouse($data, $status, $lastkey);
    $status = collect_previous_click($data, $status, $lastkey);
    # Key combos, special keys and mouse buttons are handled differently
    # so we will deal with this keypress in the scope of a typing event.
    # Use the textbuffer to store its value.
    my $textbuffer = $status->{textbuffer};
    $textbuffer = use_text_buffer($status, $currentvalue, $textbuffer);
    # Update status
    $status->{textbuffer} = $textbuffer;
    return ($currentkey, $status);
}

sub resolve_modpress {
# This subroutine resolves the modifier press. It will collect mouse and typing events
# first and then deal with the modifiers.
    my ($data, $status, $lastkey, $currentkey) = @_;
    my ($currentaction, $currentvalue) = ($data->{$currentkey}[0], $data->{$currentkey}[1]);
    my ($lastaction, $lastvalue) = ($data->{$lastkey}[0], $data->{$lastkey}[1]);
    my $typedtext;
    # Collect previous events.
    $status = collect_previous_mouse($data, $status, $lastkey);
    $status = collect_previous_click($data, $status, $lastkey);
    # When the $status is not modified, it means that this is the first modifier
    # ever used. We will remember it and set the $status to modified and put
    # the modifier into the text buffer.
    if ($status->{modified} == 0) {
        # Resolve possible keyboard events.
        $status = collect_previous_keys($data, $status, $lastkey);
        # Put the modifier in the textbuffer.
        my $textbuffer = $status->{textbuffer};
	push (@$textbuffer, $currentvalue);
	$status->{textbuffer} = $textbuffer;
        $status->{modified} = 1;
    }
    # If we already are modified, the we expect a key combo with multiple modifiers,
    # so we store this one in the textbuffer, too.
    else {
        my $textbuffer = $status->{textbuffer};
        push (@$textbuffer, $currentvalue);
	$status->{textbuffer} = $textbuffer;
    }
    return ($currentkey, $status);

}

sub resolve_modrelease {
# This routine will deal with a modifier release. As any modifier release is preceded
# by a modifier press, it is impossible that there are no modifiers pressed at the moment.
# Therefore this action will always finish the pressed modifiers and delete them from
# the buffer. In case of multiple modifier pressed, we will assume that any modifier
# released will immediately finish the whole event.
    my ($data, $status, $lastkey, $currentkey) = @_;
    my ($currentaction, $currentvalue) = ($data->{$currentkey}[0], $data->{$currentkey}[1]);
    # Collect the combo from the textbuffer only when the first ever modifier was released.
    my $textbuffer = $status->{textbuffer};
    if ($textbuffer->[0] eq $currentvalue) {
	my $combo = join("-", @$textbuffer);
	my $action = [$lastkey, "keycombo", $combo];
    	$status = action_into_status($status, $action);
    	# Delete the textbuffer.
    	$status->{textbuffer} = [];
	$status->{modified} = 0;
    }
    return ($currentkey, $status);
}

sub resolve_specpress {
# This routine deals with special keys pressed. There are several possibilities.
# We will describe them later on.
    my ($data, $status, $lastkey, $currentkey) = @_;
    my ($currentaction, $currentvalue) = ($data->{$currentkey}[0], $data->{$currentkey}[1]);
    my ($lastaction, $lastvalue) = ($data->{$lastkey}[0], $data->{$lastkey}[1]);
    my $written;
    # If the last event before this one was a keyrelease
    if ($lastaction eq "keyrelease") {
    	$status = collect_previous_keys($data, $status, $lastkey);
    }
    # In case of previous modpress, the special key will be recorded in the textbuffer 
    # as part of the modified key combo. If we are not modified, collect the
    # previous key actions.
    elsif ($lastaction eq "modpress") {
    	if ($status->{modified}) {
	    my $textbuffer = $status->{textbuffer};
	    push(@$textbuffer, $currentvalue);
	    $status->{textbuffer} = $textbuffer;
	}
    }

    # SysRQ is a special key that is returned when PrtScr key is pressed.
    # In Gnome, this will trigger the screenshotting application. On Wayland,
    # taking screenshots via this application is the only way how to do this
    # reliably. The previous version of AutoCoconut tried to take shots permanently
    # which slowed down the process. 
    #
    # Now, we can use this key to take screenshots (if needed) while we are recording 
    # the moves without the recording being cluttered with SysRQ calls.
    # In Gnome, taking the screenshot is confirmed with the press of the Enter key.
    # So, if there is a sequence of SysRQ and Enter, we will inform about 
    # a screenshot taken.
    #
    # FIXME: This will only work with Gnome. Later some better logic
    # will be needed. If SysRq is pressed, then change the status
    # to screenshot.
    elsif ($currentvalue eq "sysrq") {
        $status->{screenshot} = 1;
    }
    # Then if previously sysrq was pressed and currently Enter is pressed
    # we know that this sequence is used for taking screenshots and we
    # will record the screenshot event.
    elsif (($lastvalue eq "sysrq") and ($currentvalue eq "enter")) {
        my $action = [$lastkey, "screenshot", "PrtScr-Enter"];
        $status = action_into_status($status, $action);
    }
    # If we are still in the screenshot mode, any Enter will abort
    # that mode.
    elsif (($lastvalue eq "enter") and ($status->{screenshot} == 1)) {
        $status->{screenshot} = 0;
    }
    else {
        # In all other cases, collect mouse events.
        $status = collect_previous_mouse($data, $status, $lastkey);
        $status = collect_previous_click($data, $status, $lastkey);
    }
    return ($currentkey, $status);
}

sub resolve_specrelease {
    # This will collect any previously recorded special key presses
    # immediately after they are released.
    my ($data, $status, $lastkey, $currentkey) = @_;
    my $lastaction = $data->{$lastkey}[0];
    if ($lastaction) {
        if ($lastaction eq "specpress") {
            # The collecting will only take place, if we are not
            # in the screenshot mode. If we are, then the any
            # special key will not collected until an Enter is
            # pressed.
            unless ($status->{screenshot}) {
                my $value = $data->{$lastkey}[1];
                my $action = [$lastkey, "keypressed", $value];
                $status = action_into_status($status, $action);
            }
        }
    }
    return ($currentkey, $status);
}

sub resolve_mousemove {
# This subroutine will resolve the mouse moves. Moving the mouse 
# is only significant, if it is connected with the mouse button
# being clicked and held which means dragging. We will therefore
# track the mouse movement, just in case, there was some dragging.
    my ($data, $status, $lastkey, $currentkey) = @_;
    my $direction = $data->{$currentkey}[1];
    my $lastaction = $data->{$lastkey}[0];
    my $lastbutton = $data->{$lastkey}[1];
    my $moves = $status->{moves};
    # If previous action is a mousepress and not mouse release,
    # there might be a mouse drag. 
    if ($lastaction eq "mousepress") {
        $status->{drag} = $lastbutton; 
    }
    if ($status->{drag} eq "") {
        $status = collect_previous_click($data, $status, $lastkey);
    }
    $moves = add_to_move($moves, $direction);
    $status->{moves} = $moves;
    return ($currentkey, $status);
}

sub resolve_mousescroll {
# This subroutine will resolve the mouse scrolls. While it should not
# be a part of any workflow to move the mouse and scroll at the same
# time, we will use the same buffer for scrolls, i.e. $moves.
    my ($data, $status, $lastkey, $currentkey) = @_;
    my $direction = $data->{$currentkey}[1];
    my $lastaction = $data->{$lastkey}[0];
    my $scroll = $status->{scrolls};
    ## Solve the previous actions:
    #if ($lastaction eq "keypress") {
    #    $status = collect_previous_keys($data, $status, $lastkey);
    #}
    #else {
    #    $status = collect_previous_mouse($data, $status, $lastkey);
    #    $status = collect_previous_click($data, $status, $lastkey);
    #}
    $status = collect_previous_click($data, $status, $lastkey);
    $scroll = add_to_move($scroll, $direction);
    $status->{scrolls} = $scroll;
    return ($currentkey, $status);
}

sub resolve_mouseclick {
# Deal with mouse clicks. Each button click finished all previous
# events and then it will deal with the current click.
# If the previous action was a mouse button release, and the click
# comes soon enough (in half a second), we will assume that we are
# dealing with a double click.
    my ($data, $status, $lastkey, $currentkey) = @_;
    my ($lastaction,$lastvalue) = ($data->{$lastkey}[0], $data->{$lastkey}[1]);
    my ($currentaction,$currentvalue) = ($data->{$currentkey}[0], $data->{$currentkey}[1]);

    $status = collect_previous_mouse($data, $status, $lastkey);
    $status = collect_previous_keys($data, $status, $lastkey);
    if ($lastaction eq "mouserelease") {
        my $timedelta = $currentkey - $lastkey;
        if (($timedelta <= 0.5) and ($currentvalue eq $lastvalue)) {
            # In case of a double click (same button pressed quickly again),
            # we will add the mouse button to the click buffer.
            my $clicks = $status->{clicks};
            push(@$clicks, $currentvalue);
            $status->{clicks} = $clicks;
        }
        else {
            # If this is not the case, then this is not a double click
            # but another single click. Therefore, we need to collect 
            # the previous click and remember this one.
            $status = collect_previous_click($data, $status, $lastkey);
            $status->{clicks} = [$currentvalue];
        }
    }
    else {
        # If here, then we only have a first click. Remember that.
        $status->{clicks} = [$currentvalue];
    }
    return ($currentkey, $status);
}

# This routine deals with mouse releases when the previous event was not
# a mousepress. In that case, we may expect that there the mouse has been
# dragged and we need to fix the event flow.
sub resolve_mouserelease {
    my ($data, $status, $lastkey, $currentkey) = @_;
    my ($lastaction,$lastvalue) = ($data->{$lastkey}[0], $data->{$lastkey}[1]);
    my ($currentaction,$currentvalue) = ($data->{$currentkey}[0], $data->{$currentkey}[1]);
    # If the last action was a mouse move    
    if ($lastaction eq "mousemove") {
        # Deal with the move direction
        my $move = $status->{moves};
        my $direction = name_moves("mousemove", $move);
        $status->{moves} = [0, 0];
        # Record the mouse drag if in drag mode. This is
        # introduced by a mouse move that follows a mouse press.
        if ($status->{drag}) {
            my $action = [$lastkey, "mousedrag", $direction];
            $status = action_into_status($status, $action);
            # The last click is still recorded, but we have replaced
            # it with the mouse drag action, so we need to destroy
            # the click in order not to be picked up by the following
            # action.
            $status->{clicks} = [];
        }
    # Each mouse release will end the drag mode immediately.    
    $status->{drag} = "";
    }
    return $status;
}

sub interpret_data {
# This method takes the simplified data from parse_recording and tries
# to group them into useful activities, such as mouse click, mouse drags, 
# wheel moves, typing, pressing single keys or key combinations.
# It works in a two-pass mode, when the event will be remembered at the first
# occurence and then compared with the following event to decide what
# kind of event this might be.
    my $data = shift;
    # As the libinput recording can only be finished with pressing the
    # Ctrl-C and the Ctrl release will not be recorded any more, 
    # we will add that ending to make sure, all events get resolved
    # properly. The same holds true for when the recording starts,
    # it seems that only the specrelease from the confirming Enter
    # key is recorded.
    my $stop = ["modrelease", "leftcontrol"];
    $data->{10000} = $stop;
    # Let's create variables to hold status quo of modifier keys
    # and also a typing buffer.
    my $status = {};
    $status->{modified} = 0; # to hold modifier status
    $status->{screenshot} = 0; # if print screen was pressed
    $status->{textbuffer} = []; # to hold single key presses
    $status->{moves} = []; # to hold mouse moves info
    $status->{scrolls} = []; # to hold mouse scrolls info
    $status->{clicks} = []; # to hold mouse button information
    $status->{typedtext} = ""; # to hold typed text
    $status->{drag} = "";
    $status->{interpreted} = [];
    my $lastkey = {};
    # Now, we should sort the data according to the timestamps. In order to
    # force Perl into a numerical sorting, we use the special numeric operator.
    my @keys = keys %$data;
    my @sorted_keys = sort {$a <=> $b} @keys;
    # We will now go through the sorted data and interpret their meaning.
    # The $key holds the time and the value holds the event info.
    for my $key (@sorted_keys) {
        # Read the event into variables. We need to dereference the event
        # as we have a passed a reference.
        my $eventaction = $data->{$key}[0]; 
        my $actionvalue = $data->{$key}[1];

        # If last seen is empty, we are just at the beginning of something,
        # so we will just add the event to the lastseen variable. The
        # first event from the recording should be the confirmation pressing
        # of the enter key, so it can just move to the last event silently.
        if (! $data->{$key}[0]) {
            say("The key in data does not exist.");
            $lastkey = $key;
        }
        else {
            # If the event is a mousemove
            if ($eventaction eq "mousemove") {
                # Handle the mousemove in a subroutine.
                ($lastkey, $status) = resolve_mousemove($data, $status, $lastkey, $key);
            }    
            elsif ($eventaction eq "mousewheel") {
                # Handle mouse scrolls in a subroutine.
                ($lastkey, $status) = resolve_mousescroll($data, $status, $lastkey, $key);
            }
            elsif ($eventaction eq "mousepress") {
                ($lastkey, $status) = resolve_mouseclick($data, $status, $lastkey, $key);
            }
            elsif ($eventaction eq "mouserelease") {
                $status = resolve_mouserelease($data, $status, $lastkey, $key);
            }
            # If the event is a regular keypress
            elsif ($eventaction eq "keypress"){
                # Handle the keypresses in a subroutine.
                ($lastkey, $status) = resolve_keypress($data, $status, $lastkey, $key);

            }
            # If the event is a modifier press.
            elsif ($eventaction eq "modpress") {
                ($lastkey, $status) = resolve_modpress($data, $status, $lastkey, $key);
            }
            # If the event is a modifier release, we will deal with the key combo.
            # The key combo should be finished, whenever a modifier is released, even
            # if the order is not the same, i.e. with ctrl-alt-k, for example, it
            # does not matter which of the modifiers is released first.
            elsif ($eventaction eq "modrelease") {
                ($lastkey, $status) = resolve_modrelease($data, $status, $lastkey, $key);    
            }
            # If the event is a special key press, than we will deal with what could
            # be placed in mouse buffers and text buffers and then will record the 
            # special key.
            elsif ($eventaction eq "specpress") {
                ($lastkey, $status) = resolve_specpress($data, $status, $lastkey, $key);
            }
            
            elsif ($eventaction eq "specrelease") {
                ($lastkey, $status) = resolve_specrelease($data, $status, $lastkey, $key);
            }

            $lastkey = $key;

        }
    }

    # Collect any leftovers from the text buffer when the iteration is over.
    my $textbuffer = $status->{textbuffer};
    $textbuffer = join("", @$textbuffer);
    
    my $interpreted = $status->{interpreted};
    return $interpreted;
}

sub translate {
    my ($data, $type, $wait) = @_;
    
    my $rewrite = {};
    #    if ($type eq "openqa") {
        $rewrite->{keypressed} = "send_key";
        $rewrite->{typetext} = "type_very_safely";
        $rewrite->{mousemove} = "mouse_set";
        $rewrite->{mousescroll} = "mousescroll";
        $rewrite->{mousedrag} = "mouse_drag";
        $rewrite->{mouseclick_single} = "assert_and_click";
        $rewrite->{mouseclick_double} = "assert_and_dclick";
        $rewrite->{mouseclick_tripple} = "mouse_tclick";
        $rewrite->{keycombo} = "send_key";
        $rewrite->{screenshot} = "needle";
        #}
        #elsif ($type eq "markdown") {
        #$rewrite->{keypressed} = "Press ";
        #$rewrite->{typetext} = "Type ";
        #$rewrite->{mousemove} = "Move mouse ";
        #$rewrite->{mousescroll} = "Scroll mouse ";
        #$rewrite->{mousedrag} = "Drag mouse ";
        #$rewrite->{mouseclick_single} = "Click mouse with ";
        #$rewrite->{mouseclick_double} = "Doubleclick mouse with ";
        #$rewrite->{mouseclick_tripple} = "Trippleclick mouse with ";
        #$rewrite->{keycombo} = "Press a key combination ";
        #$rewrite->{screenshot} = "Screenshot was taken ";
        #}

    my $keyrename = {};
    $keyrename->{leftcontrol} = "ctrl";
    $keyrename->{leftalt} = "alt";
    $keyrename->{leftshift} = "shift";
    $keyrename->{rightcontrol} = "ctrl";
    $keyrename->{rightshift} = "shift";
    $keyrename->{rightalt} = "alt";
    $keyrename->{enter} = "ret";
    $keyrename->{leftmeta} = "super";
    $keyrename->{rightmeta} = "meta";
    $keyrename->{kp1} = "1";
    $keyrename->{kp2} = "2";
    $keyrename->{kp3} = "3";
    $keyrename->{kp4} = "4";
    $keyrename->{kp5} = "5";
    $keyrename->{kp6} = "6";
    $keyrename->{kp7} = "7";
    $keyrename->{kp8} = "8";
    $keyrename->{kp9} = "9";
    $keyrename->{kp0} = "0";
    $keyrename->{kpplus} = "+";
    $keyrename->{kpdot} = ".";
    $keyrename->{kpcomma} = ",";
    $keyrename->{kpequal} = "equal";
    $keyrename->{dot} = ".";
    $keyrename->{comma} = ",";
    $keyrename->{kpenter} = "ret";
    $keyrename->{kpminus} = "minus";
    $keyrename->{space} = "spc";
    $keyrename->{kpasterisk} = "*";
    $keyrename->{pagedown} = "pgup";
    $keyrename->{pageup} = "pgdn";
    $keyrename->{leftmousebutton} = "left";
    $keyrename->{rightmousebutton} = "right";
    $keyrename->{middlemousebutton} = "middle";

    if ($type eq "markdown") {
        $keyrename->{leftmousebutton} = "left button";
        $keyrename->{rightmousebutton} = "right button";
        $keyrename->{middlemousebutton} = "middle button";
        $keyrename->{enter} = "enter";
    }

    my @modifiers = qw(rightcontrol leftcontrol leftalt rightalt rightshift leftshift leftmeta rightmeta);

    my $translated_lines = [];

    # Go through the events and create commands
    for my $event (@$data) {
        my $command = $event->[1];
        # Rewrite command for openQA
        $command = $rewrite->{$command};
        my $value = $event->[2];
        # For combos, we need to decompose the combo and rewrite modifiers.
        if ($value =~ /-/) {
            my @combo = split("-", $value);
            foreach (@combo) {
                my @k = keys(%$keyrename);
                if ($_~~ @k) {
                    $_ = $keyrename->{$_};
                }
            }
            $value = join("-", @combo);
        }
        my @keys = keys %$keyrename;
        # Rewrite values for openQA
        if ($value ~~ @keys) {
            $value = $keyrename->{$value};
        }
        # Each command need some parametres. Depending on the command
        # add parameteres to it.
        my $line;
        my $opvalue;
        if ($command eq "type_very_safely") {
            $opvalue = "(\"$value\");";
            $line = $command.$opvalue;
            if ($type eq "markdown") {$line = "Type $value"};
            push(@$translated_lines, $line);
        }
        elsif ($command eq "send_key") {
            $opvalue = "(\"$value\");";
            $line = $command.$opvalue;
            if ($type eq "markdown") {$line = "Press $value."};
            push(@$translated_lines, $line);
        }
        elsif ($command eq "mouse_set") {
            $line = "# Mouse was moved $value but we do not track coordinates. If you need to position the mouse, use: ".'mouse_set($x, $y).';
            if ($type eq "markdown") {$line = "Move mouse $value."};
            push(@$translated_lines, $line);
        }
        elsif ($command eq "mousescroll") {
            $line = "# Mouse was scrolled $value, but openQA does not support mouse scrolls, use different means to navigate across the page.";
            if ($type eq "markdown") {$line = "Scroll mouse $value."};
            push(@$translated_lines, $line);
        }
        elsif ($command eq "assert_and_click") {
            $opvalue = "(\"name_of_needle\", button => \"$value\", timeout => $wait);";
            $line = $command.$opvalue;
            if ($type eq "markdown") {$line = "Click mouse with $value."};
            push(@$translated_lines, $line);
        }
        elsif ($command eq "assert_and_dclick") {
            $opvalue = "(\"name_of_needle\", button => \"$value\", timeout => $wait);";
            $line = $command.$opvalue;
            if ($type eq "markdown") {$line = "Doubleclick mouse with $value."};
            push(@$translated_lines, $line);
        }
        elsif ($command eq "mouse_tclick") {
            $opvalue = '($x, $y); # There is no assertion wrapper command, but you can use this for tripple clicks.';
            $line = $command.$opvalue;
            if ($type eq "markdown") {$line = "Trippleclick mouse with $value."};
            push(@$translated_lines, $line);
        }
        elsif ($command eq "needle") {
            $line = "# Needle taken while recording. Check the Screenshot location.";
            if ($type eq "markdown") {$line = "Screenshot taken and saved to the screenshot directory."};
            push(@$translated_lines, $line);
        }
	    elsif ($command eq "mouse_drag") {
	        $line = "mouse_drag(\"from_needle\", \"to_needle\"); # Exact coordinates could not be calculated. Take needles to identify positions.";
            if ($type eq "markdown") {$line = "Drag mouse $value."};
	        push(@$translated_lines, $line);
	    }
    }
return $translated_lines;
}

#####################################

# Read the CLI argument for the filename
my %cli = @ARGV;
my $filename = $cli{-f};
my $outfile = $cli{-o};
my $translator = $cli{-t};
my $wait = $cli{-w};
# If no translator is given, assume openQA
# and assume 30 second time out if nothing is
# given.
$translator = "openqa" if (! $translator);
say "Translator is $translator.";
$wait = "30" if (! $wait);

# Load the recording from the YAML file.
my $data = load_yaml_from_disk($filename);

# Parse the data from the YAML file.
my $newdata = parse_recording($data);
# Interpret the events and create meta events.
my $status = interpret_data($newdata);
# Translate for output.
my $translated;
if ($translator eq "openqa") {
    $translated = translate($status, "openqa", $wait);
}
elsif ($translator eq "markdown") {
    $translated = translate($status, "markdown", 0);
}
else {
    # This should never become true.
    say "Translator not chosen, skipping translation.";
    $translated = {};
}

# Write out the translated file.

my $opening = <<END;
use base "installedtest";
use strict;
use testapi;
use utils;

sub run {
    my \$self = shift;
END

if ($translator eq "markdown") {
    $opening = <<END;
# AutoCoconut Workflow

END
}

my $closing = <<END;
}

sub test_flags {
    return {fatal => 1};
}

1;
END

if ($translator eq "markdown") {
    $closing = "";
}

if ($outfile) {
    open(OUTFILE, '>', $outfile) or die $!;
    print OUTFILE $opening;
    for my $line (@$translated) {
        print OUTFILE "    ";
        print OUTFILE $line;
        print OUTFILE "\n";
    }
    print OUTFILE $closing;
    close(OUTFILE);
    say("The translation has been saved as $outfile.");
}
else {
    say($opening);
    for my $line (@$translated) {
        if ($translator eq "openqa") {
            print("    ");
        }
        say($line);
    }
    say($closing);
}

