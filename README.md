# AutoCoconut (version 2)
AutoCoconut is a Perl script that takes a `libinput` recording, parses it and translates 
the single events obtained from the recording into meta events which can be used to generate
an openQA test scripts based on the obtained recording.

It differs from the original version of AutoCoconut because it does not try
to collect mouse and keyboard events from the system directly, but uses a `libinput`
recording that is much more reliable than the previous version of AutoCoconut
itself and also works on Wayland without any limitations which makes it
a better fit for the future. 

## Requirements

* Perl with **perl-YAML-Syck** package
* **libinput** and **libinput-utils** if you want to create the recordings. To install the requirements quickly, you can use the `install_deps.pl` script.

~~~
$ sudo install_deps.pl
~~~

## Libinput

`libinput` is used to create the workflow recordings. Normally, `libinput-utils` that are required for the application to work are not installed, so you might need to install that package. To obtain the recording from `libinput`, use:

1. Get the list of all connected input devices that can be recorded to choose which devices you would like to record

   ~~~
   $ sudo libinput list-devices
   ~~~
2. On a usual system, there might be multiple input devices connected and you should make sure that you are recording the devices you want to record. To check if a specific device is producing input into the system, run 

   ~~~
   $ sudo libinput debug-events /dev/input/event10`
   ~~~

   where **10** is the number of the device. Note, that these numbers will vary on your system. If you can see events being displayed as you are using the device, you have identified the device correctly.
3. To start the recording:

   ~~~
   $ sudo libinput record /dev/input/event10 -o recording.yml --show-keycodes
   ~~~

   Note the importance of the option `--show-keycodes`. Without this, the key and mouse button events are obfuscated and you will not be able to recover the value of those buttons later anyhow.

4. Stop the recording using the **Ctrl-C** key combination. This stop combination will also be recorded in the workflow.

## The `autococonut` script.

The script converts the recording into either a markdown workflow (not implemented yet) or an openQA test module. To convert the recording use:

~~~
$ ./autococonut.pl -f recording.yml -t openqa -w 30
~~~

The new content will be created in a new file, with the same name as the input file and a new suffix, depending which translator is used to finish the process.

Note, that it will be always necessary to **edit** the test module before you attempt to run it inside openQA, but this approach could save you quite a lot of time working on the tests.

### Arguments explained

* -f takes the libinput recording YAML file. When omitted, the script will ask for the filename interactively.
* -o takes the output file. When omitted, the output will be printed to the CLI.
* -t takes the translator (openqa, markdown), currently only openqa is supported. When omitted, openqa translator will be used.
* -w takes the timeout time. Some openQA needles require timeout to be set, you can pre-set it here (in seconds). When omitted, 30 seconds will be used which is the default time for openQA if not timeout is used with the commands. 

### openQA needles

openQA uses needles (screenshots with pre-defined areas) that are used to match the system being tested with what is expected. Due to Wayland limitations it is not possible to take the screenshots automatically as you record the input, but there is a workaround, at least for Gnome Desktop.

Whenever you want to take the screenshot, press the **PrtSc** key which will start Gnome screenshot application, press **Enter** to record the screenshot. On Gnome desktop, it will be saved in `~/Pictures/Screenshots` where you can find it later and use it to create the needle off-line using the **Needly** application, for example, that you can find at https://github.com/lruzicka/needly.

The recording sequence **PrtSc** with **Enter** pressed right after it **will not** be recorded into the workflow. Instead a comment appears in
the output file informing you about the existence of the screenshot.

To take screenshots correctly, keep the following guidelines:

1. If you want to take a screenshot, do it before you move the mouse to the widget. Do not take screenshots while mouse is hovering above the widget as it may affect the appearance of the widget and openQA will not be able to match it correctly. 
2. Note, that for openQA certain resolution is required. For Fedora openQA, this resolution is **1024x768**. Screenshots that will not have this exact resolution, will not be recognized by openQA and you will need to retake them in the online application. 
3. To take screenshots in a given resolution, either change it on your system before you start recording, or do the recording in a virtual machine with a correct resolution set. If you do not plan taking screenshots, you do not need to bother with the resolution.
4. Gnome screenshot application remembers the last setting. Make sure you have set it before you start recording, because if you will attempt to mingle with the setting during the recording, the screenshotting sequence will not be recognized correctly and the setting up process will be recorded as part of the workflow.

## Current status quo

The script is **partly production ready** at the moment. The majority of features is already implemented, but there still may be bugs.
Testing is taking place.

## Combinations to test

* Single mouse events  (PASSED)
  
  * left click (PASSED)
  * middle click (PASSED)
  * right click (PASSED)
  * mouse move (PASSED)
  * mouse drag (PASSED)

* Single keyboard events (PASSED)
  
  * one key press (PASSED)
  * one modifier press (PASSED)
  * one special key press (PASSED)

* Keyboard combinations
  
  * typing (PASSED)
  * key combo (PASSED)
  * key combo with special keys (PASSED)
  * key combo with multiple modifiers (PASSED)
  * key combo with one modifier and multiple letters (PASSED)
  * key combo with multiple modifiers and multiple letters (PASSED)

* Mouse combinations
  
  * double click (PASSED)
  * tripple click (PASSED)
  * mouse drag and mouse click (PASSED)
  * mouse click and mouse drag (PASSED)
  * mouse double click and mouse drag (PASSED)
  * mouse tripple click and mouse drag (PASSED)

* Sequences
  * typing and mouse click (PASSED)
  * typing and special key (PASSED)
  * typing and key combo (PASSED)
  * typing and mouse drag
  * mouse click and key combo (PASSED)
  * mouse click and special key (PASSED)
  * special key and key combo (PASSED)
  * mouse click and typing (PASSED)
  * special key and typing (PASSED)
  * key combination and typing (PASSED)

## To do

1. Create a translator for Markdown.
2. Create a translator for AsciiDoc.

## Reporting issues

If you find a problem with application, report it, please.

