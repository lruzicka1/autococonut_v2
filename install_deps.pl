#!/usr/bin/perl

use warnings;
use strict;
use feature qw(say);

say("Installing Perl dependencies:");
system("dnf install -y perl-YAML-Syck");

say("Installing the Libinput packages:");
system("dnf install -y libinput libinput-utils");

say("Installations finished.");


